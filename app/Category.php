<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'title', 
        'alias',
    ];

    

    public function wallpapers() {
    	return $this->belongsToMany('App\Wallpaper')->withTimestamps();
    }

    public function languages() {
    	return $this->belongsToMany('App\Language')->withTimestamps();
    }

}
