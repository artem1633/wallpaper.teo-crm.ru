<?php

namespace App\Http\Controllers;

use App\Wallpaper;
use App\Category;
use App\Language;

use Illuminate\Http\Request;

class ApisController extends Controller
{
    public function getAllLanguagesApi() 
    {

        $languages = Language::latest('id')->get();
        return response()->json($languages);

    }

    public function getAllCategoriesApi($lang) 
    {

        $current_lang = Language::select()->where('alias', $lang)->first();
        $categories = $current_lang->categories()->get();

        return response()->json($categories);

    }

    public function getAllWallpapersApi($page = 0) 
    {

        if($page == 0){
            $page = 1;
        }
        if ($page != 0) {
            if ($page == 1) {
                $offset = 0;
            }
            else {
                $offset = 20 * ($page - 1);
            }
        }

        if ($page != 0) {
            $wallpapers = Wallpaper::inRandomOrder('1234')->limit(20)->offset($offset)->get();
        }
        else {
            $wallpapers = Wallpaper::inRandomOrder()->limit(20)->get();
        }
        
        $domain = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
        
        foreach ($wallpapers as $key => $wallpaper) {

            $wallpaper->original = $domain . "/files/images/" . $wallpaper->source;
            $wallpaper->thumb = $domain . "/files/thumbs/" . $wallpaper->source;
        }

        return response()->json($wallpapers);

    }

    public function getAllWallpapersOfCategoryApi($category, $page = 0) {


        $current_category = Category::select()->where('alias', $category)->first();


        if ($page != 0) {
            if ($page == 1) {
                $offset = 0;
            }
            else {
                $offset = 20 * ($page - 1);
            }
        }

        if ($page != 0) {
            $wallpapers = $current_category->wallpapers()->latest('id')->limit(20)->offset($offset)->get();
        }
        else {
            $wallpapers = $current_category->wallpapers()->inRandomOrder()->limit(20)->get();
        }


        //$wallpapers = $current_category->wallpapers()->limit(5)->get();
        $domain = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];

        foreach ($wallpapers as $key => $wallpaper) {
            $wallpaper->original = $domain . "/files/images/" . $wallpaper->source;
            $wallpaper->thumb = $domain . "/files/thumbs/" . $wallpaper->source;
        }
        return response()->json($wallpapers);

    }

    public function getSingleWallpaperApi($id) {

        $wallpaper = Wallpaper::find($id);
        return response()->json($wallpaper);

    }

    public function getRandomWallpaper($category)
    {
        $current_category = Category::select()->where('alias', $category)->first();
        $wallpaper = $current_category->wallpapers()->inRandomOrder()->first();
        $domain = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
		$wallpaper->original = $domain . "/files/images/" . $wallpaper->source;
        $wallpaper->thumb = $domain . "/files/thumbs/" . $wallpaper->source;
        return response()->json($wallpaper);
    }
}
