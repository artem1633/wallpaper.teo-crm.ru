<?php

namespace App\Http\Controllers;

use App\Wallpaper;
use App\Category;
use App\Language;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{

	public function __construct() 
    {
        $this->middleware('auth'); 
    }

    public function index() 
    {
        $categories = Category::latest('id')->paginate(8);

    	return view('categories.index', compact('categories'));
    }

    public function show($alias) 
    {
        //dd($alias);
    	$category = Category::select()->where('alias', $alias)->first();

    	if(is_null($category)) {
    		abort(404);
    	}
    	
    	return view('categories.index', compact('category'));
    }

    public function create() 
    {
        $languages = Language::latest('id')->pluck('title', 'id');
    	return view('categories.create', compact('languages'));
    }


    public function store(Request $request)  
    {

        $category = new Category($request->all());

        $validatedData = $request->validate([
            'title' => 'required|min:3',
            'alias' => 'required|unique:categories|min:3',
        ]);



        $category->save();

        $lang_ids = $request->input('languages');

        $category->languages()->attach($lang_ids);

        return redirect('admin/categories');
    
    }

    public function edit($id) 
    {
        $languages = Language::latest('id')->pluck('title', 'id');
        $category = Category::select()->where('id', $id)->first();
        return view('categories.edit', compact('category', 'languages'));
    }

    public function update($id, Request $request) 
    {

        $category = Category::select()->where('id', $id)->first();

        $validatedData = $request->validate([
            'title' => 'required|min:3',
            'alias' => 'required|min:3',
        ]);

        $lang_ids = $request->input('languages');
        $category->languages()->sync($lang_ids);

        $category->update($request->all());

        return redirect('admin/categories');
    }

    public function delete($id) 
    {
        $category = Category::find($id);
        $category->delete();
        return redirect('admin/categories');
    }
}
