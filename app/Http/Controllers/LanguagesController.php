<?php

namespace App\Http\Controllers;

use App\Language;
use App\Category;
use Illuminate\Http\Request;

class LanguagesController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth'); 
    }

    public function index() 
    {
        $languages = Language::latest('id')->paginate(8);

    	return view('languages.index', compact('languages'));
    }

    public function show($alias) 
    {
        //dd($alias);
    	$language = Language::select()->where('alias', $alias)->first();

    	if(is_null($language)) {
    		abort(404);
    	}
    	
    	return view('languages.index', compact('language'));
    }

    public function create() 
    {
    	return view('languages.create');
    }


    public function store(Request $request)  
    {

        $category = new Language($request->all());

        $validatedData = $request->validate([
            'title' => 'required|min:3',
            'alias' => 'required|unique:categories|min:2',
        ]);

        $category->save();

        return redirect('admin/languages');
    
    }

    public function edit($id) 
    {
        $language = Language::select()->where('id', $id)->first();
        return view('languages.edit', compact('language'));
    }

    public function update($id, Request $request) 
    {

        $language = Language::select()->where('id', $id)->first();

        $validatedData = $request->validate([
            'title' => 'required|min:3',
            'alias' => 'required|min:2',
        ]);

        $language->update($request->all());

        return redirect('admin/languages');
    }

    public function delete($id) 
    {
        $language = Language::find($id);
        $language->delete();
        return redirect('admin/languages');
    }
}
