<?php

namespace App\Http\Controllers;

use App\Wallpaper;
use App\Category;
use App\Language;
use Image;
use File;
use Response;
use Session;

//use Filesystem;

//use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;

class WallpapersController extends Controller
{

    public function __construct() 
    {
        $this->middleware('auth'); 
    }

    public function index() 
    {
        $wallpapers = Wallpaper::latest('id')->paginate(8);

    	return view('wallpapers.index', compact('wallpapers'));
    }


    public function show($alias) 
    {
        //dd($alias);
    	$wallpaper = Wallpaper::select()->where('alias', $alias)->first();

    	if(is_null($wallpaper)) {
    		abort(404);
    	}
    	
    	return view('wallpapers.index', compact('wallpaper'));
    }


    public function create() 
    {
    	$categories = Category::latest('id')->pluck('title', 'id');
    	return view('wallpapers.create', compact('categories'));
    }


    public function create_thumb($source, $filename) {

        $size = getimagesize($source);

        if ($size[0] >= $size[1]){
            $relashion = $size[0] / $size[1];
            $height = round(500 / $relashion, 0);
            Image::make($source)->resize(500, $height)->save(public_path('/files/thumbs/' . $filename));
        }
        else {
            $relashion = $size[1] / $size[0];
            $width = round(500 / $relashion, 0);
            Image::make($source)->resize($width, 500)->save(public_path('/files/thumbs/' . $filename));
        }

    }


    public function store(Request $request)  
    {

        if($request->hasFile('source'))
        {

            $sources = $request->file('source');

            foreach ($sources as $i => $source) {
                $wallpaper = new Wallpaper();
                
                //$salt = random_int(10, 99);
                $salt = md5($source->getClientOriginalName());
                $dbImg = Wallpaper::where('source', '=', $salt.'.'.$source->getClientOriginalExtension())->first();
                if($dbImg){
                    return \Redirect::back()->with('error', 'Some images are existes in database');
                }

                $filename = $salt . '.' . $source->getClientOriginalExtension();
                Image::make($source)->save(public_path('/files/images/' . $filename));
                $wallpaper->source = $filename;

                //$validated = $request->validated();
                $wallpaper->save();

                WallpapersController::create_thumb($source, $filename);

                $cat_ids = $request->input('categories');
                $wallpaper->categories()->attach($cat_ids);

                //$wallpaper->save();

            }
        }

        return redirect('admin/wallpapers');

    }


    public function edit($id) 
    {

        $categories = Category::latest('id')->pluck('title', 'id');

        $wallpaper = Wallpaper::select()->where('id', $id)->first();
        $source = $wallpaper->source;

        return view('wallpapers.edit', compact('wallpaper', 'categories'));
    }


    public function update($id, Request $request) 
    {

        $wallpaper = Wallpaper::select()->where('id', $id)->first();
        
        $cat_ids = $request->input('categories');
        $wallpaper->categories()->sync($cat_ids);

        return redirect('admin/wallpapers');
    }


    public function delete($id) 
    {
        $wallpaper = Wallpaper::find($id);
        $filename = $wallpaper->source;
        File::delete(public_path('/files/images/'.$filename));
        File::delete(public_path('/files/thumbs/'.$filename));
        $wallpaper->delete();
        return redirect('admin/wallpapers');
    }

    public function massdelete(Request $request)
    {
        $wallids = explode(',', $request->wallids);
        foreach($wallids as $id){
            $wallpaper = Wallpaper::find($id);
            if($wallpaper){
                $filename = $wallpaper->source;
                File::delete(public_path('/files/images/'.$filename));
                File::delete(public_path('/files/thumbs/'.$filename));
                $wallpaper->delete();
            }
        }
        return redirect('admin/wallpapers');
    }

}
