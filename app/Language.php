<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    
	protected $fillable = [
        'title', 
        'alias',
    ];

    public function categories() {
    	return $this->belongsToMany('App\Category')->inRandomOrder();
    }
}
