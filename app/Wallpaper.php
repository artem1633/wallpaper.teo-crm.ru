<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallpaper extends Model
{
    protected $fillable = [
        'source',
    ];



    public function categories() 
    {
        return $this->belongsToMany('App\Category')->withTimestamps();
    }
}
