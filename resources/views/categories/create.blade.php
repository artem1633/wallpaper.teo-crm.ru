@extends('home')

@section('content')
		<div class="container">
			<h1>Новая категория</h1>
				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				<hr>
						{!! Form::open(['url' => 'admin/categories']) !!}
							
							
							@include('categories.form', ['submitButtonText' => 'Добавить категорию'])

							
						{!! Form::close() !!}

	</div>
@stop