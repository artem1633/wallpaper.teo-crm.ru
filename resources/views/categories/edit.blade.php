@extends('home')

@section('content')
	<h1>Редактирование: {!! $category->title !!}</h1>
	@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
	@endif
	<hr>
						{!! Form::model($category, ['method' => 'PATCH', 'action' => ['CategoriesController@update', $category->id]]) !!}
							
							@include('categories.form', ['submitButtonText' => 'Изменить категорию'])

						{!! Form::close() !!}

						

@stop