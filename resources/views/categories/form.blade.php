<div class="form-group">

								{!! Form::label('title', 'Название:') !!}
								{!! Form::text('title', null, ['class' => 'form-control']) !!}

							</div>		

							<div class="form-group">

			                	{!! Form::label('languages', 'Язык:') !!}
			                	{!! Form::select('languages[]', $languages, null, ['class' => 'form-control form-categories-blck', 'multiple']) !!}
			              
			              	</div>

							<div class="form-group">
							
								{!! Form::label('alias', 'Алиас:') !!}
								{!! Form::text('alias', null, ['class' => 'form-control']) !!}
							
							</div>


							<div class="form-group">
							
								{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
							
							</div>