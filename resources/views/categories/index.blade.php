@extends('home')

@section('content')
	
<div class="row">
	<h1>Категории</h1>
	<a class="btn btn-success" href="{{ url('/admin/categories/create') }}">Добавить категорию</a>
	<hr>	
	<table class="table">
		<thead>
			<th>id</th>
			<th>Название</th>
			<th>Язык</th>
			<th>Алиас</th>
			<th>Управление</th>
		</thead>
		<tbody>
			@foreach ($categories as $category)
				<tr>
					<td>{{ $category->id }}</td>
					<td>{{ $category->title }}</td>
					<td>
						@foreach ($category->languages as $language) 
							<a class="tag" href="/admin/languages">{{ $language->title }}</a><br> 
						@endforeach
					</td>
					<td>{{ $category->alias }}</td>
					<td>
						<a class="btn-primary btn-sm" href="/admin/categories/{{ $category->id }}/edit">Изменить</a> 
						<a class="btn-danger btn-sm" href="/admin/categories/{{ $category->id }}/delete">Удалить</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>	
	{{ $categories->appends(request()->except('page'))->links() }}			
</div>

@stop