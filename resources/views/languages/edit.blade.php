@extends('home')

@section('content')
	<h1>Редактирование: {!! $language->title !!}</h1>
	@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
	@endif
	<hr>
						{!! Form::model($language, ['method' => 'PATCH', 'action' => ['LanguagesController@update', $language->id]]) !!}
							
							@include('languages.form', ['submitButtonText' => 'Изменить категорию'])

						{!! Form::close() !!}

						

@stop