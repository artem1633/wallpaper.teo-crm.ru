<div class="form-group">

								{!! Form::label('title', 'Название:') !!}
								{!! Form::text('title', null, ['class' => 'form-control']) !!}

							</div>		

							<div class="form-group">
							
								{!! Form::label('alias', 'Алиас:') !!}
								{!! Form::text('alias', null, ['class' => 'form-control']) !!}
							
							</div>

							<div class="form-group">

							</div>

							<div class="form-group">
							
								{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
							
							</div>