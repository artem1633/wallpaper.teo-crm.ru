@extends('home')

@section('content')
	
		<div class="row">
				<h1>Языки</h1>
	<a class="btn btn-success" href="{{ url('/admin/languages/create') }}">Добавить язык</a>
<hr>

	

	<table class="table">
		<thead>
			<th>id</th>
			<th>Название</th>
			<th>Алиас</th>
			<th>Управление</th>
		</thead>
		<tbody>
			@foreach ($languages as $language)
				<tr>
					<td>{{ $language->id }}</td>
					<td>{{ $language->title }}</td>
					<td>{{ $language->alias }}</td>
					<td>
						<a class="btn-primary btn-sm" href="/admin/languages/{{ $language->id }}/edit">Изменить</a> 
						<a class="btn-danger btn-sm" href="/admin/languages/{{ $language->id }}/delete">Удалить</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>	

			
		</div>

@stop