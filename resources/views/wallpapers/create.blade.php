@extends('home')

@section('content')
		<div class="container">
			
			<h1>Добавление изображений</h1>
			@if(Session::has('error'))
			<p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
			@endif
			
				<hr>
						{!! Form::open(['url' => 'admin/wallpapers', 'enctype' => 'multipart/form-data']) !!}
							
							
							@include('wallpapers.form', ['submitButtonText' => 'Добавить изображение'])

							
						{!! Form::close() !!}

	</div>
@stop