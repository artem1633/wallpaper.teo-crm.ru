@extends('home')

@section('content')
	<h1>Редактирование: {!! $wallpaper->title !!}</h1>
	<hr>
						{!! Form::model($wallpaper, ['method' => 'PATCH', 'action' => ['WallpapersController@update', $wallpaper->id]]) !!}
							
							@include('wallpapers.form', ['submitButtonText' => 'Изменить Изображение'])

						{!! Form::close() !!}

						

@stop