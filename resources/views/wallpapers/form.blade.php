
							<div class="form-group">
							
							@if(isset($wallpaper))
								<div class="article-thumb-img" style="background-image: url(/files/thumbs/{{ $wallpaper->source }}); width: 300px; height: 200px; background-size: cover;"></div> 
								<br>
							@endif
							@if(!isset($wallpaper))	
							<div class="form-group">

								{!! Form::label('source', 'Основное изображение:') !!}
								{!! Form::file('source[]', ['multiple' => 'multiple'], ['class' => 'form-control']) !!}
    
							</div>
							@endif
							<div class="form-group">

			                	{!! Form::label('categories', 'Категории:') !!}
			                	{!! Form::select('categories[]', $categories, null, ['class' => 'form-control form-categories-blck', 'multiple']) !!}
			              
			              	</div>

							</div>

							<div class="form-group">

							</div>

							<div class="form-group">
							
								{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
							
							</div>