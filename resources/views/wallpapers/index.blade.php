@extends('home')

@section('content')
	
		<div class="row">
				<h1>Изображения</h1>
	<a class="btn btn-success" href="{{ url('/admin/wallpapers/create') }}">Добавить изображение</a>
<hr>

	

	<table class="table">
		<thead>
		<th><!--<input type="checkbox" id="all">--> </th>
			<th>id</th>
			<th>Название</th>
			<th>Категории</th>
			<th>Превью</th>
			<th colspan="2">Ссылки</th>
			<th>Управление</th>
		</thead>
		<tbody>
			@foreach ($wallpapers as $wallpaper)
				<tr>
					<td><input type="checkbox" name="images[]" value="{{ $wallpaper->id }}" class="wall-id"> </td>
					<td>{{ $wallpaper->id }}</td>
					<td>{{ $wallpaper->source }}</td>
					<td>
						@foreach ($wallpaper->categories as $category) 
							<a class="tag" href="/admin/categories">{{ $category->title }}</a><br> 
						@endforeach
					</td>
					<td><img src="/files/images/{{ $wallpaper->source }}" width="100px"></td>
					<td><a target="_blank" href="/files/thumbs/{{ $wallpaper->source }}">Ссылка на превью</td>
					<td><a target="_blank" href="/files/images/{{ $wallpaper->source }}">Ссылка на оригинал</td>
					<td>
						<a class="btn-primary btn-sm" href="/admin/wallpapers/{{ $wallpaper->id }}/edit">Изменить</a> 
						<a class="btn-danger btn-sm" href="/admin/wallpapers/{{ $wallpaper->id }}/delete">Удалить</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
			<form class="form-inline">
				<select class="form-control" onchange="delimg($(this))">
					<option value="">С отмеченными</option>
					<option value="del">Удалить</option>
				</select>
			</form>


{{ $wallpapers->appends(request()->except('page'))->links() }}
			
		</div>
		<div style="display: none">
			<form id="massdelete" method="post" action="/admin/wallpapers/massdelete">
				{{ csrf_field() }}
				<input type="hidden" id="wallids" name="wallids">
			</form>
		</div>
<script>
	function delimg(select){
	    var wall_ids = [];
	    if(select.val() == 'del'){
	        $('input:checkbox:checked').each(function(){
				wall_ids.push(this.value);
			});
	        $("#wallids").val(wall_ids);
	        $("#massdelete").submit();
            //console.log(wall_ids);
		}
	}
</script>
@stop