<?php


use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Controller;
use Collective\Html\FormFacade;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('admin/wallpapers');
});

Route::get('/doc_for_api', function () {
    return view('doc_for_api');
});

Route::get('/languages_list', 'ApisController@getAllLanguagesApi');
Route::get('/categories_list/{lang}', 'ApisController@getAllCategoriesApi');
Route::get('/wallpapers_list', 'ApisController@getAllWallpapersApi');
Route::get('/category/{category}', 'ApisController@getAllWallpapersOfCategoryApi');
Route::get('/wallpapers_list/{page}', 'ApisController@getAllWallpapersApi');
Route::get('/category/{category}/{page}', 'ApisController@getAllWallpapersOfCategoryApi');
Route::get('/wallpaper/{id}', 'ApisController@getSingleWallpaperApi');
Route::get('/wallpaper_random/{category}','ApisController@getRandomWallpaper');

Route::get('/home', function () {
    return redirect('admin/wallpapers');
});

Route::get('/admin', 'HomeController@index')->name('home');
Auth::routes();

Route::get('/register', function () {
    return redirect('admin/wallpapers');
});

Route::resource('/admin/wallpapers', 'WallpapersController'); 
Route::get('admin/wallpapers/{id}/delete', 'WallpapersController@delete');
Route::post('admin/wallpapers/massdelete', 'WallpapersController@massdelete');

Route::resource('/admin/categories', 'CategoriesController'); 
Route::get('admin/categories/{id}/delete', 'CategoriesController@delete');

Route::resource('/admin/languages', 'LanguagesController'); 
Route::get('admin/languages/{id}/delete', 'LanguagesController@delete');
